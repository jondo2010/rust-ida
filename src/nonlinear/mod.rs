pub mod newton;
pub mod traits;

pub use newton::Newton;
pub use traits::*;
